#!/bin/bash

dirname="$(dirname "$0")"

"$dirname/setup-selinux.sh"

# Start rsyslogd:
{
	echo 'module(load="imuxsock" SysSock.Use="on")'
	echo 'kern.*   /dev/null'
} >/tmp/rsyslog.conf || exit 1
rsyslogd -f /tmp/rsyslog.conf || exit 1
[ -S /dev/log ] || exit 1 # We need the /dev/log socket

# Use locally built audit-userspace:
INSTALL_DIR="$(readlink -f audit-userspace/install)"
export PATH="$INSTALL_DIR/bin:$INSTALL_DIR/sbin:$PATH"
export LD_LIBRARY_PATH="$INSTALL_DIR/lib:$LD_LIBRARY_PATH"

# Check version:
which auditd
ausearch --version || exit 1

# Start auditd:
mkdir -p /tmp/etc/audit || exit 1
mkdir -p /var/log/audit || exit 1
cp "$dirname/auditd.conf" /tmp/etc/audit/auditd.conf || exit 1
mount --bind /tmp/etc/audit /etc/audit
auditd -s disable || exit 1
auditctl -D || exit 1
auditctl -e 1 || exit 1

echo $(id -u) > /proc/self/loginuid || exit 1
