#!/bin/bash

JOBS=$(nproc)
JOBS=${JOBS:-1}

obj_path="$(readlink -f obj)"
out_path="$(readlink -f out)"

export CC="ccache gcc"

make -C src O="$obj_path" olddefconfig || exit 1
make -C src O="$obj_path" -j$JOBS C=1 W=1 "$@" || exit 1
make -C src O="$obj_path" install "$@" \
	INSTALL_PATH="$out_path" INSTALL_MOD_PATH="$out_path"
make -C src O="$obj_path" modules_install "$@" \
	INSTALL_MOD_PATH="$out_path"
