#!/bin/bash

set -e

dirname="$(dirname "$0")"

cd "$dirname"

git -C src remote add selinux https://git.kernel.org/pub/scm/linux/kernel/git/pcmoore/selinux.git
git -C src fetch selinux
git -C worktrees/main/src worktree add ../../selinux/src --checkout --track -b selinux-next selinux/next

# "install" the Qt Creator project file
env -C worktrees/main/src ln -s ../../../kernel.pro.txt kernel.pro
