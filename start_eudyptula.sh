#!/bin/bash

if [ -z "$KERNEL_VERSION" ]; then
    KERNEL_VERSION="$(make -C obj kernelrelease | grep -v 'make:')" || {
        echo 1>&2 "ERROR: Unable to determine kernel version!"
        exit 1
    }
fi

echo 1>&2 "$0: Detected kernel version: $KERNEL_VERSION"

KERNEL_PATH="out/vmlinuz-$KERNEL_VERSION"

[ -f "$KERNEL_PATH" ] || KERNEL_PATH="$KERNEL_PATH+"

./eudyptula-boot.sh -o -m 1G -d work -k "$KERNEL_PATH" \
    --net --cmdline=selinux=permissive --qemu="-cpu host -smp cores=2,threads=2,sockets=1" \
    $@
