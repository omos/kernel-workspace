#!/bin/bash

set -e

dirname="$(dirname "$0")"

cd "$dirname"

sudo dnf install -y \
	flex bison gcc make findutils perl-interpreter openssl openssl-devel elfutils-libelf-devel bc \
	busybox sparse ccache ncurses-devel \
	bzip2 rpm-build koji qemu \
	git-core

mkdir out
mkdir work
mkdir -p worktrees/main/src

git -C worktrees/main/src init .
git -C worktrees/main/src config extensions.worktreeConfig true

./switch-tree.sh main

# Set up basic remotes
git -C src remote add origin https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
git -C src remote add stable https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git
git -C src remote add stable-rc https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git
git -C src remote add next https://git.kernel.org/pub/scm/linux/kernel/git/next/linux-next.git
git -C src remote add ark https://gitlab.com/cki-project/kernel-ark.git

git -C src fetch --multiple origin ark

# Set up worktrees and basic branches
git -C worktrees/main/src checkout --track -B master origin/master
git -C worktrees/main/src worktree add ../../ark/src --checkout --track -b os-build ark/os-build

# Add required ARK workaround branches
git -C src fetch https://gitlab.com/omos/kernel-ark.git ark-lookaside-tarball-dl:ark-lookaside-tarball-dl
git -C src fetch https://gitlab.com/omos/kernel-ark.git ark-fix-buildreq-check:ark-fix-buildreq-check
