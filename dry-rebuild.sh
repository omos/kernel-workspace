#!/bin/bash

JOBS=$(nproc --all)
JOBS=${JOBS:-1}

make -C src O=$(readlink -f obj) -j$JOBS "$@" || exit 1
