#!/bin/bash

function common() {
	distro="$1"; shift

	bkr workflow-tcms --distro "$distro" --distro-tag CTS_NIGHTLY \
		--whiteboard "[kernel] $distro | $mode : $koji_type $koji_task | $*" \
		--suppress-install-task --first-task /distribution/check-install \
		--first-testing-task=/tools/toolchain-common/Install/reboot \
		"$@"
}

distro="$1"; shift
mode="$1"; shift

case "$mode" in
brew|koji)
	server="$mode"
	koji_type="$1"; shift
	koji_task="$1"; shift
	common "$distro" --brew-server "$server" --brew-method multi \
		"--brew-$koji_type" "$koji_task" "$@"
	;;
cki)
	pipeline_id="$1"; shift
	repo_url=""
	echo 1>&2 "ERROR: Not implemented yet!"
	;;
*)
	echo 1>&2 "ERROR: Invalid mode!"
esac
