#!/bin/bash

set -e

COMMIT_ID_REGEX='^commit [0-9a-f]{40}$|^\(cherry picked from commit [0-9a-f]{40}\)$'

mbox="$1"; shift

grep -E "$COMMIT_ID_REGEX" $mbox | grep -Eo '[0-9a-f]{40}'
