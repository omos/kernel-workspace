#!/bin/bash

name="$1"; shift

if [ -z "$name" ]; then
	echo 1>&2 "usage: $1=0 NAME"
	exit 1
fi

if ! [ -f localversion ]; then
	echo 1>&2 "ERROR: Must be in a RH kernel build directory!"
	exit 1
fi

# --arch x86_64
make rh-brew \
	BUILDID=".$name" \
	BREW_FLAGS=" --fail-fast" \
	BUILDOPTS="+up -debug -debuginfo -doc"
