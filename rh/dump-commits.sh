#!/bin/bash

set -e

for arg in "$@"; do
	git show -s --format='%H' --reverse "$arg"
done
