#!/bin/bash

set -e

bugzilla="$1"; shift
upstream_status="$1"; shift

[ ! -z "${bugzilla##*[!0-9]*}" ] || exit 1

export NO_REBUILD=1
export NO_VERIFY=1

for arg in "$@"; do
	for commit in $(git show -s --format='%H' --reverse "$arg"); do
		flags=''
		conflict=0
		if ! env EDITOR=: git cherry-pick --no-edit $commit; then
			conflict=1
			flags+=' --edit'
			while true; do
				echo "Please resolve conflict, starting shell..."
				bash -i || { git cherry-pick --abort; exit 1; }
				EDITOR=: git cherry-pick --continue && break
			done
		fi
		git commit --reset-author --signoff --amend $flags -F <(
			git log --pretty='%s' --decorate=no $commit^..$commit
			echo
			echo "Bugzilla: https://bugzilla.redhat.com/show_bug.cgi?id=$bugzilla"
			if [ -n "$upstream_status" ]; then
				echo "Upstream Status: $upstream_status"
			fi
			if [ $conflict -eq 1 ]; then
				echo "Conflicts:"
				echo "  - TODO"
			fi
			echo
			git log --pretty=medium --decorate=no $commit^..$commit
			echo
		)
	done
done
