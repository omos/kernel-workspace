#!/bin/bash

set -e

COMMIT_LEN=9

fixes_re="^Fixes: ("
for commit in `cat`; do
	fixes_re+="$(head -c $COMMIT_LEN <<<"$commit")|"
done
fixes_re="${fixes_re%?})"

git fetch origin
git log -E --grep "$fixes_re" origin/master
