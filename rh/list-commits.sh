#!/bin/bash

set -e

for arg in "$@"; do
	for commit in $(git show -s --format='%H' --reverse "$arg"); do
		git --no-pager log --oneline --decorate=no $commit^..$commit
	done
done
