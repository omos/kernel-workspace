#!/bin/bash

set -e

good="$1"; shift
bad="$1"; shift

git bisect start "$bad" "$good"

# Currently optimized for kABI bisecting:
echo ".kabitest" >localversion
git bisect run make rh-brew \
	BREW_FLAGS="--arch x86_64 --fail-fast" \
	BUILDOPTS="+up -debug -debuginfo -doc -tools -selftests -perf -bpftool"
