#!/bin/bash

dirname="$(dirname "$0")"

"$dirname/run-beaker-tests.sh" "$@" --task /CoreOS/audit/Sanity/audit-testsuite
