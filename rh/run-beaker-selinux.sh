#!/bin/bash

dirname="$(dirname "$0")"

# --task /kernel/distribution/selinux-testsuite

"$dirname/run-beaker-tests.sh" "$@" \
	--plan 27115
