#!/bin/bash

# Checks if a set of commits (e.g. "abc bcd..def 012..345") is already
# backported in the current branch.

set -e

regex=""
for arg in "$@"; do
	for commit in $(git show -s --format='%H' --reverse "$arg"); do
		regex+="$commit|"
	done
done

regex="${regex%"|"}"

if [ -n "$regex" ]; then
	git --no-pager log --oneline --decorate=no --grep "$regex"
fi
