#!/bin/bash

dirname="$(dirname "$0")"
tree="$1"

[ -d "$dirname/worktrees/$tree" ] || exit 1

ln -sfT "$tree" "$dirname/worktrees/current"
ls -l "$dirname/worktrees/current"
