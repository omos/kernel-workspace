#!/bin/bash

set -e

git push -d public "$@" || true

for branch in "$@"; do
	git branch -m "$branch" "archive-$branch"
done
