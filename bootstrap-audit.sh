#!/bin/bash

set -e

dirname="$(dirname "$0")"

cd "$dirname"


git -C src remote add audit https://git.kernel.org/pub/scm/linux/kernel/git/pcmoore/audit.git
git -C src fetch audit
git -C worktrees/main/src worktree add ../../audit/src --checkout --track -b audit-next audit/next
