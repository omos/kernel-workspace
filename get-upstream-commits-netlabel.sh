#!/bin/bash

git -C src log --pretty='format:%H	%s' --no-merges --no-decorate "$@" -- \
	Documentation/netlabel/ \
	include/net/calipso.h \
	include/net/cipso_ipv4.h \
	include/net/netlabel.h \
	include/uapi/linux/netfilter/xt_SECMARK.h \
	include/uapi/linux/netfilter/xt_CONNSECMARK.h \
	net/netlabel/ \
	net/ipv4/cipso_ipv4.c \
	net/ipv6/calipso.c \
	net/netfilter/xt_CONNSECMARK.c \
	net/netfilter/xt_SECMARK.c
