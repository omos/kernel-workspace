#!/bin/bash

set -e

dirname="$(dirname "$0")"

cd "$dirname"

mkdir out
mkdir work
mkdir -p worktrees/main/src

git -C worktrees/main/src init .

git -C src remote add centos-9 https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9.git
git -C src remote add rhel-9 git@gitlab.com:redhat/rhel/src/kernel/rhel-9.git
git -C src remote add rhel-8 git@gitlab.com:redhat/rhel/src/kernel/rhel-8.git
git -C src remote add rhel-7 git@gitlab.com:redhat/rhel/src/kernel/rhel-7.git
git -C src remote add rhel-6 git@gitlab.com:redhat/rhel/src/kernel/rhel-6.git

git -C src fetch --multiple centos-9 rhel-9 rhel-8 rhel-7 rhel-6

git -C worktrees/main/src worktree add ../../rhel6/src --checkout --track -b rhel-6.y rhel-6/main
git -C worktrees/main/src worktree add ../../rhel7/src --checkout --track -b rhel-7.y rhel-7/main
git -C worktrees/main/src worktree add ../../rhel8/src --checkout --track -b rhel-8.y rhel-8/main
git -C worktrees/main/src worktree add ../../rhel9/src --checkout --track -b centos-9.y centos-9/main
