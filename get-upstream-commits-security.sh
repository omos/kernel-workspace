#!/bin/bash

git -C src log --pretty='format:%H	%s' --no-merges --no-decorate "$@" -- \
	security/selinux/ \
	scripts/selinux/ \
	include/linux/selinux* \
	security/commoncap.c \
	security/inode.c \
	security/lsm_audit.c \
	security/min_addr.c \
	security/security.c \
	security/Makefile \
	security/Kconfig \
	include/linux/lsm_* \
	include/linux/security.h
