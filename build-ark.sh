#!/bin/bash

# A helper script to build a Fedora scratch kernel using the ARK tree

set -ex

export GIT_PAGER=cat
export NO_VERIFY=1

buildid="$1"; shift
mode="baseonly"
if [ $# -ge 1 ]; then
	mode="$1"; shift
fi
compiler="gcc"
if [ $# -ge 1 ]; then
	compiler="$1"; shift
fi

[ -n "$buildid" ] || {
	echo "ERROR: You have to specify a buildid!" 1>&2
	exit 1
}

function cleanup() {
	set +ex
	[ -n "$tmpwt" ] && git worktree remove -f "$tmpwt"
	[ -n "$tmpdir" ] && rm -rf "$tmpdir"
	return 0
}

trap cleanup EXIT

tmpdir="$(mktemp -d)"

git fetch ark
git fetch origin
git branch -f master origin/master

head="$(git rev-parse --verify HEAD)"

os_build=ark/os-build
if false; then
ark_base="$(git merge-base "$os_build" "$head")"
ark_head="$(git log --pretty=format:%H --reverse --ancestry-path --first-parent \
	"$ark_base..$os_build" | head -n 1)"
# HACK
ark_head=kernel-5.12.0-0.rc2.165

git worktree add --detach "$tmpdir" "$ark_head"
tmpwt="$tmpdir"
elif true; then
# This would build a "vanilla"-style kernel:
git worktree add --detach "$tmpdir" "$head"
tmpwt="$tmpdir"

env -C "$tmpdir" git merge --no-stat --no-edit "ark/ark-infra"
else
# And this builds a Fedora-style kernel:
git worktree add --detach "$tmpdir" ark/os-build
tmpwt="$tmpdir"

env -C "$tmpdir" git reset --soft ark/master
env -C "$tmpdir" git commit -m 'Apply os-build differences'
env -C "$tmpdir" git rebase --onto "$head" ark/master HEAD
fi

# TODO drop ark-fix-buildreq-check once MR 1786 is merged
cherries='ark-lookaside-tarball-dl ark-fix-buildreq-check' # ark-disable-debuginfo
for cherry in $cherries; do
	env -C "$tmpdir" git cherry-pick "$os_build..$cherry"
done

if false; then
env -C "$tmpdir" git merge --no-stat --no-edit "$head"

env -C "$tmpdir" git diff --stat HEAD^
fi

env -C "$tmpdir" sed -i 's/%define with_headers 0//' redhat/kernel.spec.template

if [ "$compiler" = "clang" ]; then
	env -C "$tmpdir" sed -i \
		's/%bcond_with toolchain_clang/%bcond_without toolchain_clang/' \
		redhat/kernel.spec.template
fi

localversion=".$buildid.${head:0:8}"
case "$mode" in
baseonly)
	debug="+up -debug"
	;;
both)
	debug="+up +debug"
	;;
debugonly)
	echo "ERROR: debugonly mode is currently broken!" 1>&2
	exit 1

	env -C "$tmpdir" sed -i 's/%define debugbuildsenabled [0-9]*/%define debugbuildsenabled 0/' redhat/kernel.spec.template
	localversion+=".dbg"
	debug="-up +debug"
	;;
*)
	echo "ERROR: Invalid mode: '$mode'!" 1>&2
	exit 1
esac

# -debuginfo
make -C "$tmpdir" dist-koji \
	BUILDID="$localversion" \
	BUILDOPTS="+headers -configchecks $debug" \
	BUILD_FLAGS="--arch x86_64 --fail-fast --nowait" \
	BUILD_SCRATCH_TARGET=f36 \
	NO_CONFIGCHECKS=1 \
	VERSION_ON_UPSTREAM=0 \
	DOWNLOAD_TARBALL=1
