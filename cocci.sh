#!/bin/bash

JOBS=$(nproc)
JOBS=${JOBS:-1}

export CC="ccache gcc"

make -C src O=$(readlink -f obj) olddefconfig || exit 1
make -C src O=$(readlink -f obj) -j$JOBS "$@" coccicheck MODE=report || exit 1
