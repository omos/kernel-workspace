#!/bin/bash

set -e

dirname="$(dirname "$0")"

[ $(getenforce) = Disabled ] || exit 0

mount -n -t selinuxfs selinuxfs /sys/fs/selinux

mkdir -p /tmp/var-lib-selinux
#cp -raP /var/lib/selinux/* /tmp/var-lib-selinux
mount --bind /tmp/var-lib-selinux /var/lib/selinux

mkdir -p /tmp/etc-selinux
cp -r "$dirname/refpolicy/out/etc/selinux/refpolicy" /tmp/etc-selinux
mount --bind /tmp/etc-selinux /etc/selinux
cat >/etc/selinux/config <<EOF
SELINUX=enforcing
SELINUXTYPE=refpolicy
EOF

semodule -DB
load_policy

mount -n -t tmpfs tmpfs /sys/fs/cgroup
mkdir /sys/fs/cgroup/unified
mount -n -t cgroup2 cgroup2 /sys/fs/cgroup/unified
# mount -n -o context=system_u:object_r:tmpfs_t:s0 -t cgroup2 cgroup2 /sys/fs/cgroup/unified
