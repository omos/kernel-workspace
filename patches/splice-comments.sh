#!/bin/bash

comments_text="$1"; shift
patch="$1"; shift

if [ $# -ne 0 ]; then
	echo 1>&2 "Too many patches!"
	exit 1
fi

PATTERN="---"
offset=$(grep -o -x -b -e "$PATTERN" "$patch" | head -n 1 | cut -d : -f 1)

tmp_file="$(mktemp)"

(( offset += ${#PATTERN} + 1 ))

{
	head -c $offset "$patch"
	echo
	cat "$comments_text"
	echo
	tail -c +$(( $offset + 1 )) "$patch"
} >"$tmp_file"

# DEBUG:
cat "$tmp_file" 1>&2

mv "$tmp_file" "$patch"
