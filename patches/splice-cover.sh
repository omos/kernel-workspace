#!/bin/bash

cover_text="$1"; shift
cover_letter="$1"; shift

SUBJ_PATTERN='*** SUBJECT HERE ***'
BODY_PATTERN='*** BLURB HERE ***'

offset_subj=$(grep -o -b "$SUBJ_PATTERN" "$cover_letter" | head -n 1 | cut -d : -f 1)
offset_body=$(grep -o -b "$BODY_PATTERN" "$cover_letter" | head -n 1 | cut -d : -f 1)

tmp_file="$(mktemp)"

(( offset_body += ${#BODY_PATTERN} + 1 ))

{
	head -c  $offset_subj "$cover_letter"
	cat "$cover_text"
	tail -c +$(( $offset_body + 1 )) "$cover_letter"
} >"$tmp_file"

# DEBUG:
cat "$tmp_file" 1>&2

# DEBUG:
cat "$@" 1>&2

mv "$tmp_file" "$cover_letter"
