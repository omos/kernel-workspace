#!/bin/bash

set -e

dirname="$(dirname "$0")"

cd "$dirname"

mkdir out
mkdir work
mkdir -p worktrees/main/src

git -C worktrees/main/src init .

./switch-tree.sh main

git -C src remote add herbert https://git.kernel.org/pub/scm/linux/kernel/git/herbert/cryptodev-2.6.git
git -C src remote add herbert-fixes https://git.kernel.org/pub/scm/linux/kernel/git/herbert/crypto-2.6.git
git -C src fetch --multiple herbert herbert-fixes
git -C worktrees/main/src worktree add ../../crypto/src --checkout --track -b crypto-next herbert/master
