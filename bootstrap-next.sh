#!/bin/bash

set -e

dirname="$(dirname "$0")"

cd "$dirname"

git -C src fetch next
git -C worktrees/main/src worktree add ../../next/src --checkout --track -b linux-next next/master
